<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class PagesController extends Controller
{
    public function index() {
        return view('home');
    }

    public function map() {
        return view('map');
    }
    
    public function mapAddress($address) {
        return view('map', ['address' => $address]);
    }

    public function addBusiness() {
        return view('add-business');
    }

    public function editBusiness($slug) {
        return view('edit-business',
            ['listing' => DB::table('crawled_data')->where('slug', $slug)->get()[0],
            'menuItems' => DB::table('menu_items')->where('listing_slug', $slug)->get()]);
    }

    public function faq() {
        return view('faq');
    }

    public function admin() {
        $pending = DB::table('crawled_data')->where('approved', '0')->get();
        return view('admin', ['pending' => $pending]);
    }

    public function profile($id) {
        $user = DB::table('users')->where('id', $id)->get();
        if (count($user) == 0) {
            abort(404); //User not found
        }

        $favorites = DB::table('favorites')->where('user_id', $id)->get();
        $favoriteListings = [];
        foreach ($favorites as $listing) {
            $listing = DB::table('crawled_data')->where('slug', $listing->listing_slug)->get();
            if (count($listing) > 0) {
                $listing = $listing[0];
                array_push($favoriteListings, [
                    'slug' => $listing->slug, 'name' => $listing->name, 'city' => $listing->city,
                    'state' => $listing->state, 'latitude' => $listing->latitude, 'longitude' => $listing->longitude
                ]);
            }
        }

        $userOwned = Auth::check() ? json_decode(Auth::user()->owned_listings, true) : null;
        $owned = [];
        if ($userOwned != null) {
            foreach($userOwned as $slug) {
                $listing = DB::table('crawled_data')->where('slug', $slug["slug"])->get();
                if (count($listing) > 0) {
                    array_push($owned, [
                        'slug' => $listing[0]->slug, 'name' => $listing[0]->name, 'city' => $listing[0]->city, 'state' => $listing[0]->state, 'approved' => $listing[0]->approved
                    ]);
                }
            }
        }

        return view('profile', ['id' => $id, 'name' => $user[0]->name, 'owned' => $owned,
            'following' => $favorites, 'listings' => $favoriteListings]);
    }
}