<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class BusinessController extends Controller {
    /**
     * Calls all requirements for adding a new business
     */
    public function addBusiness() {
        if (!Auth::check()) {
            die("You must be logged in to submit this listing!");
        }

        $this->validateMenuItemImages(Input::all());
        $validator = $this->validateBusiness(Input::all());
        if ($validator->fails()) {
            die("Validation failed on the server. Please do not modify the DOM before submitting the form.");
        } else {
            $this->updateDBEntry(Input::all()); //Send to DB
            echo("1"); //Return 1 on success
        }
    }

    /**
     * Calls all requirements for updating a business
     */
    public function updateBusiness() {
        if (!Auth::check()) {
            die ("You must be logged in to update this listing!");
        }

        $this->validateMenuItemImages(Input::all());
        $validator = $this->validateBusiness(Input::all());

        if ($validator->fails()) {
            die("Validation failed on the server. Please do not modify the DOM before submitting the form.");
        } else {
            $this->updateDBEntry(Input::all());
            return "1";
        }
    }

    /**
     * Validates the passed data against validation rules for
     * businesses.
     * @param $data Data to validate against
     * @return Validator for passed data
     */
    private function validateBusiness($data) {
        return Validator::make($data, [
            'name' => 'required|max:225|min:5',
            'description' => 'required|max:2500',
            'address' => 'required|max:225',
            'city' => 'required|max:225',
            'zip' => 'required|max:225',
            'latitude' => 'required|max:225',
            'longitude' => 'required|max:225',
            'logo' => 'mimes:png,jpg,jpeg,tiff,gif|max:10240', //Max upload size: 10MB
            'item-name' => 'max:225',
            'item-description' => 'max:225'
        ]);
    }

    /**
     * Sends business information to server
     * @param $data data to send
     */
    private function updateDBEntry($data) {
        //TODO: Update on server side, check notebook for changes to make
        $age = $data["age"] == "18" ? "eighteen_plus" : "twenty_one_plus";
        $userId = $this->getUserID();
        $hours = $this->generateHoursJSON($data);
        $avatar = isset($data["logo"]) && $this->isValidImageSize($data["logo"]) ? $this->submitAvatar($data) : "";

        $row = [
            'name' => $data['name'],
            'body' => $data['description'],
            'address' => $data['address'],
            'city' => $data['city'],
            'zip_code' => $data['zip'],
            $age => true,
            'license_type' => $data['type'],
            'lat_lon' => $data['latitude'] . "," . $data['longitude'],
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude'],
            'email_address' => isset($data['email']) ? $data['email'] : "",
            'phone_number' => isset($data['phone']) ? $data['phone'] : "",
            'approved' => false,
            'owner' => $userId,
            'avatar' => $avatar
        ];

        if ($data["edited-hours"] == true) array_push($row, ["hours" => $hours]);

        DB::table('crawled_data')->where("slug", $data['slug'])->update($row);
        $this->updateMenuItems($data, $data["slug"]);
    }

    /**
     * Sends the data to the database
     * @param $data data to submit
     */
    private function submitDBEntry($data) {
        $age = $data["age"] == "18" ? "eighteen_plus" : "twenty_one_plus";
        $slug = $this->generateSlug($data['name']);
        $userId = $this->getUserID();
        $hours = $this->generateHoursJSON($data);
        $avatar = isset($data["logo"]) && $this->isValidImageSize($data["logo"]) ? $this->submitAvatar($data) : "";

        $row = [
            'name' => $data['name'],
            'body' => $data['description'],
            'address' => $data['address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'zip_code' => $data['zip'],
            $age => true,
            'license_type' => $data['type'],
            'lat_lon' => $data['latitude'] . "," . $data['longitude'],
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude'],
            'email_address' => isset($data['email']) ? $data['email'] : "",
            'phone_number' => isset($data['phone']) ? $data['phone'] : "",
            'approved' => false,
            'owner' => $userId,
            'slug' => $slug,
            'hours' => $hours,
            'avatar' => $avatar
        ];

        DB::table('crawled_data')->insert($row); //Send to database!
        $this->submitMenuItems($data, $slug);
    }

    /**
     * Generates a slug for the passed listing name
     * @param $name name to generate slug from
     * @return string string representation of generated url slug
     */
    private function generateSlug($name) {
        $count = 1;
        $slug = preg_replace('/\s+/', '-', $name);

        while (DB::table('crawled_data')->where('slug', $slug)->count() > 0) {
            $slug = preg_replace('/\s+/', '-', $name) . "-" . $count;
            $count ++;
        }

        return $slug;
    }

    /**
     * @return mixed The id of the user who uploaded this listing. Dies if not found.
     */
    private function getUserID() {
        $email = Auth::user()->email;
        $user = DB::table('users')->where('email', $email)->get();

        if (!isset($user[0])) {
            die("Your email doesn't exist in database.");
        } else {
            return $user[0]->id;
        }
    }

    /**
     * Generates the JSON data needed for hours
     * @param $data Data to pull hour data from
     * @return mixed empty array if no hours where checked or a json string
     */
    private function generateHoursJSON($data) {
        $days = ['sunday' => 'sun',
            'monday' => 'mon',
            'tuesday' => 'tue',
            'wednesday' => 'wed',
            'thursday' => 'thu',
            'friday' => 'fri',
            'saturday' => 'sat'];

        $jsonArr = [];

        foreach ($days as $day => $abbrDay) {
            $dayOpen = $day . '_open';

            if (isset($data[$dayOpen])) { //Open on this $day
                $openAMPM = substr($data[$day . '_o'], -1) == "a" ? "am" : "pm";
                $closeAMPM = substr($data[$day . '_c'], -1) == "a" ? "am" : "pm";
                $openTime = substr($data[$day . '_o'], 0, -1) . ":00";
                $closeTime = substr($data[$day . '_c'], 0, -1) . ":00";

                array_push($jsonArr, [
                    'opening_time' => $openTime . $openAMPM,
                    'closing_time' => $closeTime . $closeAMPM,
                    'day_of_the_week' => $abbrDay
                ]);
            }
        }

        $formattedJSON = json_encode($jsonArr);
        return $formattedJSON;
    }

    /**
     * Prepares uploaded avatar and inserts it into the file system.
     * Resizes and centers image.
     * @param $data data that contains image to upload
     * @return Path to submitted avatar
     */
    private function submitAvatar($data) {
        $path = $data['logo']->store('avatars');
        return $path;
    }

    /**
     * Confirms that the uploaded image passes size requirements.
     * @param $image Data containing image to check
     * @return bool true or false depending on whether the image passes the requirements
     */
    private function isValidImageSize($image) {
        $filePath = $image->getRealPath();
        list($imgWidth, $imgHeight) = getimagesize($filePath);

        if ($imgWidth / $imgHeight == 1) {
            return true;
        } else {
            die("Your logo must have square dimensions (eg: 150px by 150px).");
        }
    }

    private function submitMenuItems($data, $listingSlug) {
        for ($i = 0; $i < count($data["item-name"]); $i++) {
            $itemImage = isset($data['item-image'][$i]) ? $this->submitItemImage($data['item-image'][$i]) : "";

            $jsonArr = [];
            if (isset($data['has-price-unit'][$i])) {
                array_push($jsonArr, ['unit' => $data['price-unit'][$i]]);
            } if (isset($data['has-price-gram'][$i])) {
                array_push($jsonArr, ['gram' => $data['price-gram'][$i]]);
            } if (isset($data['has-price-2gram'][$i])) {
                array_push($jsonArr, ['2gram' => $data['price-2gram'][$i]]);
            } if (isset($data['has-price-eighth'][$i])) {
                array_push($jsonArr, ['eighth' => $data['price-eighth'][$i]]);
            } if (isset($data['has-price-half-ounce'][$i])) {
                array_push($jsonArr, ['half-ounce' => $data['price-half-ounce'][$i]]);
            } if (isset($data['has-price-ounce'][$i])) {
                array_push($jsonArr, ['ounce' => $data['price-ounce'][$i]]);
            }

            $item = [
                'name' => $data["item-name"][$i],
                'body' => $data['item-description'][$i],
                'item_slug' => $this->generateItemSlug($data['item-name'][$i], $listingSlug),
                'type' => $data['item-type'][$i],
                'has_image' => isset($data['item-image'][$i]) ? 1 : 0,
                'listing_slug' => $listingSlug,
                'prices' => json_encode($jsonArr),
                'image' => $itemImage
            ];

            DB::table('menu_items')->insert($item);
        }
    }

    private function updateMenuItems($data, $slug) {
        //First, remove old menu items
        $results = DB::table('menu_items')->where('listing_slug', $slug)->get();
        foreach ($results as $listing) {
            $foundListing = false;

            for ($i = 0; $i < count($results); $i++) {
                if ($data['item-name'][$i] == $listing['name'][$i]) {
                    $foundListing = true;
                }
            }

            if (!$foundListing) {
                DB::table('menu_items')
                    ->where('listing_slug', $slug)
                    ->where('name', $listing['name'][$i])
                    ->delete();
            }
        }

        for ($i = 0; $i < count($data["item-name"]); $i++) {
            $jsonArr = [];
            if (isset($data['has-price-unit'][$i])) {
                array_push($jsonArr, ['unit' => $data['price-unit'][$i]]);
            }
            if (isset($data['has-price-gram'][$i])) {
                array_push($jsonArr, ['gram' => $data['price-gram'][$i]]);
            }
            if (isset($data['has-price-2gram'][$i])) {
                array_push($jsonArr, ['2gram' => $data['price-2gram'][$i]]);
            }
            if (isset($data['has-price-eighth'][$i])) {
                array_push($jsonArr, ['eighth' => $data['price-eighth'][$i]]);
            }
            if (isset($data['has-price-half-ounce'][$i])) {
                array_push($jsonArr, ['half-ounce' => $data['price-half-ounce'][$i]]);
            }
            if (isset($data['has-price-ounce'][$i])) {
                array_push($jsonArr, ['ounce' => $data['price-ounce'][$i]]);
            }

            $item = [
                'name' => $data["item-name"][$i],
                'body' => $data['item-description'][$i],
                'type' => $data['item-type'][$i],
                'has_image' => isset($data['item-image'][$i]) ? 1 : 0,
                'prices' => json_encode($jsonArr)
            ];

            if (isset($data['item-image'])) {
                $img = $this->submitItemImage($data['item-image'][$i]);
                array_push($item, ["item-image" => $img]);
            }

            DB::table('menu_items')
                ->where('listing_slug', $slug)
                ->where('item_slug', $data['item-slug'][$i])
                ->update($item);
        }
    }

    private function generateItemSlug($itemName, $listingSlug) {
        $count = 1;
        $slug = preg_replace('/\s+/', '-', $itemName);

        while (DB::table('menu_items')->where(['listing_slug' => $listingSlug, 'item_slug' => $slug])->count() > 0) {
            $slug = preg_replace('/\s+/', '-', $itemName) . "-" . $count;
            $count ++;
        }

        return $slug;
    }

    private function submitItemImage($image) {
        $path = $image->store('menu-item-images');
        return $path;
    }

    private function validateMenuItemImages($data) {
        foreach ($data["item-image"] as $file) {
            if (Validator::make([$file], ['item-image' => 'mimes:png,jpg,jpeg,tiff,gif|max:10240'])->fails()) {
                die ("Your menu item images cannot be bigger than 10MB");
            }
        }
    }
}