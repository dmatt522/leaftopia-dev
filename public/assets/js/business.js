/**
 * Created by CChristie on 11/15/2016.
 */

//-- Create map --//
var map;
function initMap() {
    var lat = "42.52249500";
    var long = "-70.91126300";

    //noinspection JSUnresolvedVariable,JSUnresolvedFunction
    map = new google.maps.Map(document.getElementById('live-map-interactive'), {
        center: {lat: parseFloat(lat) , lng: parseFloat(long)},
        zoom: 14
    });

    //Disable interaction
    map.setOptions({
        draggable: false,
        zoomControl: false,
        scrollWheel: false,
        disableDoubleClickZoom: true,
        disableDefaultUI: true
    });
}

//-- Manage hour checkboxes --//
$("input[type='checkbox']").click(function() {
    var isChecked = $(this).is(':checked');
    var $row = $(this).parent().parent();

    if (isChecked) {
        $row.find("select").removeAttr("disabled");
    } else {
        $row.find("select").attr("disabled", "");
    }
});


//-- Preview loaded images --//
var $changedElement = undefined;
$(document).on("change", "input[type='file']", function() {
    $changedElement = $(this);
    var input = this;

    if (input.files && input.files[0]) { //Contains a file
        var reader = new FileReader();

        reader.onload = function(e) { //Image loaded
            var containerStyle = "height: 125px; width: 125px; background-position: center;" +
                "background-image: url(" + e.target.result + ")";

            var containerSelector = $changedElement.attr("preview");
            var $container = $changedElement.parent().parent().find(containerSelector);
            $container.attr("style", containerStyle);
        };

        reader.readAsDataURL(input.files[0]); //Load image from network
    } else {
        $("#logo-container").attr("style", "");
    }
});

//-- Validate --//
$.validate();

//-- Submit --//
$("#submit").click(function(e) {
    if ($(this).isValid()) {
        e.preventDefault();
        console.log("Valid, submitting form");

        submitData("submit");
    }
});

//-- Update --//
$("#update").click(function(e) {
    if ($(this).isValid()) {
        e.preventDefault();
        console.log("Valid, submitting form");

        submitData("update");
    }
});

$("#select-country").blur(function() {
    if ($(this).val() != "US") {
        $("#select-state").attr("disabled", "");
    } else {
        $("#select-state").removeAttr("disabled");
    }
});

//-- Map lat / lon picker --//
var triggerMapElements = "#street-address, #city-name, #select-country, #select-state, #zip-code";
$(triggerMapElements).blur(function() {
    if(addressComponentsValid()) { //Content found, try geocoding
        geocodeAddress();
    }
});

/**
 * Geocodes the address components present in the DOM
 * input elements.
 */
function geocodeAddress() {
    var state = "";
    if ($("#select-state").length > 0) {
        state = $("#select-state")[0].hasAttribute("disabled") ? "" : " " + $("#select-state").val();
    } else {
        state = $(".state").attr("state");
    }

    var country = $("#select-country").length > 0 ? $("#select-country").val() : "";
    var address = $("#street-address").val() + " " + $("#city-name").val() + " " + state + " " + $("#zip-code").val() + " " + country;
    var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=AIzaSyCCV0UScujyBhP1js2g5oLjV_ss9mOJiRc";
    $.getJSON(url, function(data) {
        if (data.results.length > 0) {
            var location = data.results[0].geometry.location;
            placeMarkerOnce(map, location.lat, location.lng);
        } else {
            console.log("Failed to geocode");
        }

        $("#latlng-button").css("display", "block"); //Show lat lng button
    });
}

/**
 * Checks whether the address elements present in
 * the DOM are valid
 */
function addressComponentsValid() {
    var elements = triggerMapElements.split(", ");
    for (var i = 0; i < elements.length; i++) {
        var $ele = $(elements[i]);
        if ($ele.is("input[type='text']") && $ele.val() == "") { //There is no content, stop
            $("#latlng-button").css("display", "block"); //Hide lat lng button
            return false;
        }
    }

    return true;
}

//-- Map manually specify lat lng --//
$("#latlng-button").one("click", function(e) {
    e.preventDefault();

    //Show input container & reset button
    $("#latlng-container").css("display", "block");
    var $reset = $("#latlng-reset").css("display", "block");

    //Change text
    $(this).html("Set Custom Coordinates");

    //Reset button clicked
    $reset.click(function(e) {
        e.preventDefault();

        if (addressComponentsValid()) {
            geocodeAddress();
        }
    });

    //Set custom coordinates clicked
    $(this).click(function(e) {
        e.preventDefault();

        var lat = $("#custom-latitude").val();
        var lng = $("#custom-longitude").val();

        if (lat != "" && lng != "") {
            placeMarkerOnce(map, parseFloat(lat), parseFloat(lng));
        }
    });
});

//-- Cache menu item and remove from DOM --//
var menuItemTemplate = $(".template").removeClass("template").parent().html();
$(".template").remove();

//-- Add a new menu item --//
$("#add-menu-item").click(function(e) {
    e.preventDefault();
    var $item = $(menuItemTemplate);

    //Show and insert
    $item.css("display", "block");
    $(".menu-items-container").append($item);
});

//-- Remove a menu item --//
$(document).on("click", ".remove-menu-item", function(e) {
    e.preventDefault();
    var $container = $(this).parent();

    //Remove container from DOM
    $container.remove();
});

//-- Show hours editor --//
$(".hours").click(function(e) {
    e.preventDefault();
    $(this).removeClass("disabled");
    $(this).find("button").remove();
});

/**
 * Submits form data to the server.
 * If an error occurs, logs it at the top. If successful,
 * redirects to the user's profile page.
 * @param type Type of submission, either "update" or "submit"
 */
function submitData(type) {
    var latlng = getMarkerLatLng();
    var editedHours = !$(".hours").hasClass("disabled");

    var itemSlug = [];
    $(".item-slug").each(function() {
       itemSlug.push($(this).attr("slug"));
    });

    var fd = new FormData($("form")[0]);
    fd.append("latitude", latlng[0]);
    fd.append("longitude", latlng[1]);
    fd.append("edited-hours", editedHours);

    if (itemSlug.length > 0) fd.append("item-slug[]", itemSlug);
    if (!$(".slug").length > 0) fd.append("slug", $(".slug").attr("slug"));

    var urlAppend = type == "update" ? "update" : "add-new";

    $.ajax({
        url: '/' + urlAppend + '-business',
        type: 'POST',
        data: fd,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
        success: function (data) {
            if (data == "1") { //Success
                window.location.replace("/profile/" + $("#user-id").attr("uid"));
            } else {
                logMessage(data, "error");
            }
        }, error: function (data) {
            logMessage(data, "error");
        }
    });
}