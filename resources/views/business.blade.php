@extends ("layouts.layout")

@section('title', 'Leaftopia')
@section('javascript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="/assets/js/map.js"></script>
@stop

@section('content')
    @if (Auth::check())
        <span id="user-id" style="display: none;" uid="{{ Auth::user()->id }}"></span>
    @endif
    <div class="header-dark">
        <div class="container">
            @include('layouts.header')
        </div>
    </div>
    <section class="dark-text">
        <div class="container">
            <div id="error-container" class="reporting error clearfix" style="display: none;">
                <span class="fa fa-close"></span><h6>Some error or success goes here</h6>
            </div>
            <h4>@yield("form-title")</h4>
            <form>
                <div class="row">
                    <div class="six columns">
                        <label>Business Name</label>
                        <input id="business-name" value="@yield('listing_name')" name="name" type="text" class="form-control full-width" placeholder="Enter a name"
                               data-validation-error-msg="Please enter a business name between 5 and 50 characters" data-validation="length"
                               data-validation-length="5-50" data-validation-error-msg-container="#name-error-container" />
                        <label class="error-message" id="name-error-container"></label>
                        <br />
                        <label>Description</label>
                        <textarea id="business-description" value="@yield('listing_description')" name="description" class="full-width" placeholder="Enter a description" cols="25"
                                  data-validation-error-msg="Your description must be shorter than 2,500 characters" data-validation="length"
                                  data-validation-length="max2500" data-validation-error-msg-container="#description-error-container"></textarea>
                        <label class="error-message" id="description-error-container"></label>
                    </div>
                    <div class="six columns">
                        <div class="row">
                            <label>Optional Logo (square dimensions required, eg: 150 by 150)</label>
                            <div class="six columns">
                                <input name="logo" type="file" accept="image/*" preview="#logo-container" />
                            </div>
                            <div class="six columns">
                                <div id="logo-container" style="background-image: url(@yield('listing_logo'))"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="six columns">
                        <label>Street Address</label>
                        <input id="street-address" value="@yield('listing_address')" name="address" type="text" class="form-control full-width" placeholder="Enter an address"
                               data-validation-error-msg="Please enter a valid address." data-validation="length required"
                               data-validation-length="max225" data-validation-error-msg-container="#address-error-container" />
                        <label class="error-message" id="address-error-container"></label>
                        <label>City</label>
                        <input id="city-name" value="@yield('listing_city')" name="city" type="text" class="form-control full-width" placeholder="Enter a city"
                               data-validation-error-msg="Please enter a valid city name." data-validation="length required"
                               data-validation-length="max225" data-validation-error-msg-container="#city-error-container"/>
                        <label class="error-message" id="city-error-container"></label>
                        <br />
                        @if (!isset($editing) || $editing == false) {{-- Can't change state while editing listing --}}
                            <label>State</label>
                            <select id="select-state" value="@yield('listing_state')" class="full-width" name="state">
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District Of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="MD">Maryland</option>
                                <option value="ME">Maine</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                            <br />
                        @else
                            <span class="state" state="{{ $listing->state }}"></span>
                        @endif
                        <label>Zip Code</label>
                        <input id="zip-code" value="@yield('listing_zip')" name="zip" type="text" placeholder="Enter a zip code"
                               data-validation-error-msg="Please enter a valid zip code." data-validation="length required"
                               data-validation-length="max225" data-validation-error-msg-container="#zip-error-container" />
                        <label class="error-message" id="zip-error-container"></label>
                    </div>
                    <div class="six columns">
                        <div id="live-map-interactive"></div>
                        <br />
                        <button id="latlng-button" class="full-width">Set custom latitude & longitude</button>
                        <button id="latlng-reset" class="full-width">Reset to address coordinates</button>
                        <div id="latlng-container" class="row">
                            <div class="six columns">
                                <label>Latitude</label>
                                <input id="custom-latitude" type="text" placeholder="Enter latitude coordinates" />
                            </div>
                            <div class="six columns">
                                <label>Longitude</label>
                                <input id="custom-longitude" type="text" placeholder="Enter longitude coordinates" />
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="six columns">
                        <label>Phone Number</label>
                        <input class="full-width" value="@yield('listing_phone')" name="phone" type="text" placeholder="Enter a phone number"
                               data-validation-error-msg="Please enter a valid phone number." data-validation="number length required"
                               data-validation-length="max225" data-validation-error-msg-container="#phone-error-container"
                               data-validation-allowing="-_" />
                        <label class="error-message" id="phone-error-container"></label>
                        <br />
                        <label>Email Address</label>
                        <input class="form-control full-width" value="@yield('listing_email')" name="email" type="email" placeholder="Enter an email address"
                               data-validation-error-msg="Please enter a valid email address." data-validation="length email required"
                               data-validation-length="max225" data-validation-error-msg-container="#email-error-container"/>
                        <label class="error-message" id="email-error-container"></label>
                        <br />
                        <label>Age Requirement</label>
                        <?php
                            $ep = isset($age18) && $age18 == false ? "checked" : "";
                            $tp = $ep === "checked" ? "" : "checked";
                        ?>
                        <div class="row">
                            <div class="six columns">
                                <input style="float: left;" type="radio" name="age" value="18" {{ $ep }}>
                                <h6 style="padding-left: 20px">18 +</h6>
                            </div>
                            <div class="six columns">
                                <input style="float: left;" type="radio" name="age" value="21" {{ $tp }}>
                                <h6 style="padding-left: 20px">21 +</h6>
                            </div>
                        </div>
                        <label>License Type</label>
                        <select class="full-width" name="type" required>
                            <option value="dispensary">Dispensary</option>
                            <option value="medical">Medical</option>
                        </select>
                    </div>
                    <?php $hourDisable = ""; if (isset($editing) && $editing == true) $hourDisable = "disabled"; ?>
                    <div class="six columns hours {{ $hourDisable }}">
                        <label>Hours (Uncheck Closed Days)</label>

                        @include("partials.hour-options", ["day" => "Sunday"])
                        @include("partials.hour-options", ["day" => "Monday"])
                        @include("partials.hour-options", ["day" => "Tuesday"])
                        @include("partials.hour-options", ["day" => "Wednesday"])
                        @include("partials.hour-options", ["day" => "Thursday"])
                        @include("partials.hour-options", ["day" => "Friday"])
                        @include("partials.hour-options", ["day" => "Saturday"])

                        @if (isset($editing) && $editing == true)
                            <br />
                            <button id="reset-hours">Change Hours</button>
                        @endif
                    </div>
                </div>
                <hr />
                <div class="row">
                    <button id="add-menu-item" class="icon-padding-right"><span class="fa fa-plus"></span>Add Menu Item</button>
                </div>
                <div class="menu-items-container">
                    <div class="menu-item-container template" style="display: none;">
                        <button class="remove-menu-item icon-padding-right"><span class="fa fa-close"></span>Remove</button>
                        <div class="row">
                            <div class="six columns">
                                <label>Item Type</label>
                                <select id="select-item-type" class="full-width" name="item-type[]">
                                    <option value="Product">Product</option>
                                    <option value="Indica">Indica</option>
                                    <option value="Preroll">Preroll</option>
                                    <option type="Sativa">Sativa</option>
                                    <option type="Hybrid">Hybrid</option>
                                </select>
                                <label>Item Name</label>
                                <input class="form-control full-width" name="item-name[]" type="text" placeholder="Enter an item name"
                                       data-validation-error-msg="Names must be between 0 and 225 characters." data-validation="length required"
                                       data-validation-length="max225" data-validation-error-msg-container="#item-name-error-container"/>
                                <label class="error-message" id="item-name-error-container"></label>
                                <label>Item Description</label>
                            <textarea id="item-description" name="item-description[]" class="full-width" placeholder="Enter a description" cols="25"
                                      data-validation-error-msg="Your description must be shorter than 2,500 characters" data-validation="length"
                                      data-validation-length="max2500" data-validation-error-msg-container="#item-description-error-container"></textarea>
                                <label class="error-message" id="item-description-error-container"></label>
                                <label>Item Image</label>
                                <div class="row">
                                    <div class="six columns">
                                        <input name="item-image[]" type="file" accept="image/*" preview=".item-image-container" />
                                    </div>
                                    <div class="six columns">
                                        <div class="item-image-container"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="six columns">
                                <label>Prices</label>
                                <div class="prices-container">
                                    <div class="row">
                                        <div class="one column">
                                            <input type="checkbox" checked name="has-price-unit[]" />
                                        </div>
                                        <div class="three columns">
                                            <label>Unit</label>
                                        </div>
                                        <div class="eight columns">
                                            <input class="form-control full-width" name="price-unit[]" type="text" placeholder="Enter the cost"
                                                   data-validation-error-msg="Please enter a number" data-validation="length number required"
                                                   data-validation-length="max225" data-validation-error-msg-container="#price-unit-error-container"/>
                                            <label class="error-message" id="price-unit-error-container"></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="one column">
                                            <input type="checkbox" checked name="has-price-gram[]" />
                                        </div>
                                        <div class="three columns">
                                            <label>Gram</label>
                                        </div>
                                        <div class="eight columns">
                                            <input class="form-control full-width" name="price-gram[]" type="text" placeholder="Enter the cost"
                                                   data-validation-error-msg="Please enter a number" data-validation="length number required"
                                                   data-validation-length="max225" data-validation-error-msg-container="#price-gram-error-container"/>
                                            <label class="error-message" id="price-unit-gram-container"></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="one column">
                                            <input type="checkbox" checked name="has-price-2gram[]" />
                                        </div>
                                        <div class="three columns">
                                            <label>Two Grams</label>
                                        </div>
                                        <div class="eight columns">
                                            <input class="form-control full-width" name="price-2gram[]" type="text" placeholder="Enter the cost"
                                                   data-validation-error-msg="Please enter a number" data-validation="length number required"
                                                   data-validation-length="max225" data-validation-error-msg-container="#price-2gram-error-container"/>
                                            <label class="error-message" id="price-2gram-error-container"></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="one column">
                                            <input type="checkbox" checked name="has-eighth-unit[]" />
                                        </div>
                                        <div class="three columns">
                                            <label>Eighth</label>
                                        </div>
                                        <div class="eight columns">
                                            <input class="form-control full-width" name="price-eighth[]" type="text" placeholder="Enter the cost"
                                                   data-validation-error-msg="Please enter a number" data-validation="length number required"
                                                   data-validation-length="max225" data-validation-error-msg-container="#price-eighth-error-container"/>
                                            <label class="error-message" id="price-eighth-error-container"></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="one column">
                                            <input type="checkbox" checked name="has-price-quarter[]" />
                                        </div>
                                        <div class="three columns">
                                            <label>Quarter</label>
                                        </div>
                                        <div class="eight columns">
                                            <input class="form-control full-width" name="price-quarter[]" type="text" placeholder="Enter the cost"
                                                   data-validation-error-msg="Please enter a number" data-validation="length number required"
                                                   data-validation-length="max225" data-validation-error-msg-container="#price-quarter-error-container"/>
                                            <label class="error-message" id="price-quarter-error-container"></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="one column">
                                            <input type="checkbox" checked name="has-price-half-ounce[]" />
                                        </div>
                                        <div class="three columns">
                                            <label>Half Ounce</label>
                                        </div>
                                        <div class="eight columns">
                                            <input class="form-control full-width" name="price-half-ounce[]" type="text" placeholder="Enter the cost"
                                                   data-validation-error-msg="Please enter a number" data-validation="length number required"
                                                   data-validation-length="max225" data-validation-error-msg-container="#price-half-ounce-error-container"/>
                                            <label class="error-message" id="price-half-ounce-error-container"></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="one column">
                                            <input type="checkbox" checked name="has-price-ounce[]" />
                                        </div>
                                        <div class="three columns">
                                            <label>Ounce</label>
                                        </div>
                                        <div class="eight columns">
                                            <input class="form-control full-width" name="price-ounce[]" type="text" placeholder="Enter the cost"
                                                   data-validation-error-msg="Please enter a number" data-validation="length number required"
                                                   data-validation-length="max225" data-validation-error-msg-container="#price-ounce-error-container"/>
                                            <label class="error-message" id="price-ounce-error-container"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>

                    @yield('menu_items')
                </div>
                <br />
                <div class="row">
                    @if (isset($editing) && $editing == true)
                        <button id="update">Update</button>
                    @else
                        <button id="submit">Submit</button>
                    @endif
                </div>
            </form>
        </div>
    </section>

    <script src="/assets/js/business.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCV0UScujyBhP1js2g5oLjV_ss9mOJiRc&callback=initMap" async defer></script>
@stop