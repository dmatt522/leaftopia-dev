<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="_token" content="{!! csrf_token() !!}" />

    <title>@yield('title')</title>

    <!-- CSS -->
    @yield('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css" rel="stylesheet" />
    <link href="/assets/css/style.css" rel="stylesheet" />

    <!-- JS -->
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script src="https://cdn.rawgit.com/STAR-ZERO/jquery-ellipsis/master/dist/jquery.ellipsis.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
    <script src="/assets/js/remodal.js"></script>
    @yield('javascript')
</head>
<body>
@yield('content')

<!-- include JS last for lazy loading -->
<script src="/assets/js/assets.js"></script>
@yield('footer')
@if (isset($showFooter) && $showFooter == true)
    <footer>
        <div class="container">
            <div class="row">
                <div class="four columns social">
                    <h5>Social</h5>
                    <div class="row">
                        <a class="clearfix" href="https://www.facebook.com/Leaftopia420/">
                            <div class="two columns">
                                <span class="fa fa-facebook fa-2x"></span>
                            </div>
                            <div class="ten columns">
                                <h6>Facebook</h6>
                            </div>
                        </a>
                    </div>
                    <div class="row">
                        <a class="clearfix" href="https://twitter.com/Leaftopia420">
                            <div class="two columns">
                                <span class="fa fa-twitter fa-2x"></span>
                            </div>
                            <div class="ten columns">
                                <h6>Twitter</h6>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="four columns text-center">
                    <h5>Navigation</h5>
                    <a href="/map"><h6>Map</h6></a>
                    @if(Auth::check()) <a href="/profile/{{ Auth::user()->id }}"><h6>Profile</h6></a>@endif
                    <a href="/add-business"><h6>Add a Dispensary</h6></a>
                    <a href="/faq"><h6>Claim a Dispensary</h6></a> <!-- TODO: Make claim listing page -->
                    <a href="#"><h6>Strains</h6></a>
                </div>
                <div class="four columns">
                    <h5>Contact</h5>
                    <h6>Email: info@leaftopia.com</h6>
                    <h6>Phone: 800-913-6675</h6>
                </div>
            </div>
        </div>
    </footer>
@endif
</body>
</html>