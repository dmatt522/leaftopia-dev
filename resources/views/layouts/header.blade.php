@section('css')
    <link href="/assets/css/remodal.css" rel="stylesheet" />
    <link href="/assets/css/remodal-default-theme.css" rel="stylesheet" />
@stop

<nav>
    <div class="row">
        @if (isset($hideSearch) && $hideSearch == true)
            <div class="six columns">
                <a href="/"><img src="/images/logo.png" id="logo" /></a>
            </div>
            <div class="six columns login-right">
                @if (Auth::check())
                    <a href="/profile/{{ Auth::id() }}">
                        <div class="profile-img">
                            <img src="/images/avatar.svg" />
                        </div>
                    </a>
                @else
                    <a href="#login-modal" data-remodal-target="login-modal" class="button u-pull-right">Login</a>
                @endif
            </div>
        @else
            <div class="two columns">
                <a href="/"><img src="/images/logo.png" id="logo" /></a>
            </div>
            <div class="eight columns">
                <div class="row">
                    <div class="ten columns">
                        <input id="map-search" type="text" placeholder="Search the map" />
                    </div>
                    <div class="two columns">
                        <button onclick="search()" type="submit" class="button full"><span class="fa fa-search"></span></button>
                    </div>
                </div>
            </div>
            <div class="two columns">
                @if (Auth::check())
                    <a href="/profile/{{ Auth::id() }}">
                        <div class="profile-img">
                            <img src="/images/avatar.svg" />
                        </div>
                    </a>
                @else
                    <a href="#login-modal" data-remodal-target="login-modal" class="button u-pull-right">Login</a>
                @endif
            </div>
        @endif
    </div>
</nav>

<div data-remodal-id="login-modal" class="auth-container">
    <div class="remodal-close-button" data-remodal-action="close">
        <span class="fa fa-close"></span>
    </div>
    <div class="row">
        <div id="login-error-container" class="reporting error clearfix" style="display: none;">
            <span class="fa fa-close"></span><h6>Some error or success goes here</h6>
        </div>
        <div class="twelve columns">
            <input id="login-email" type="text" placeholder="Email Address" />
            <input id="login-password" type="password" placeholder="Password" />
            <a id="login-button" type="button" class="button full">Login</a>
        </div>
    </div>
    <br />
    <div class="row auth-options">
        <div class="six columns">
            <a href="#lost-password-modal">Forgot Password</a>
        </div>
        <div class="six columns">
            <a href="#register-modal">Register</a>
        </div>
    </div>
</div>

<div data-remodal-id="register-modal" class="auth-container">
    <div class="remodal-close-button" data-remodal-action="close">
        <span class="fa fa-close"></span>
    </div>
    <div class="row">
        <div id="register-error-container" class="reporting error clearfix" style="display: none;">
            <span class="fa fa-close"></span><h6>Some error or success goes here</h6>
        </div>
        <div class="twelve columns">
            <input id="register-name" type="text" placeholder="Display Name" />
            <input id="register-email" type="text" placeholder="Email Address" />
            <input id="register-password" type="password" placeholder="Password" />
            <input id="register-password-conf" type="password" placeholder="Confirm Password" />
            <a id="register-button" type="button" class="button full">Sign Up</a>
        </div>
    </div>
    <br />
    <div class="row auth-options">
        <div class="six columns">
            <a href="#lost-password-modal">Forgot Password</a>
        </div>
        <div class="six columns">
            <a href="#login-modal">Login</a>
        </div>
    </div>
</div>

<div data-remodal-id="lost-password-modal" class="auth-container">
    <div class="remodal-close-button" data-remodal-action="close">
        <span class="fa fa-close"></span>
    </div>
    <div class="row">
        <div id="recover-error-container" class="reporting error clearfix" style="display: none;">
            <span class="fa fa-close"></span><h6>Some error or success goes here</h6>
        </div>
        <div class="twelve columns">
            <input type="text" placeholder="Email Address" />
            <a id="recover-button" type="button" class="button full">Send Recovery Email</a>
        </div>
    </div>
    <br />
    <div class="row auth-options">
        <div class="six columns">
            <a href="#register-modal">Register</a>
        </div>
        <div class="six columns">
            <a href="#login-modal">Login</a>
        </div>
    </div>
</div>

@section('footer')
    <script>
        $('[data-remodal-id=login-modal]').remodal({ });
        $('[data-remodal-id=register-modal]').remodal({ });
        $('[data-remodal-id=lost-password-modal]').remodal({ });

        //Ajax requests for login, registration, & recover password
        $("#login-button").click(function() {
            $.ajax({
                url: '/login',
                type: 'POST',
                data: {
                    "email" : $("#login-email").val(),
                    "password" : $("#login-password").val()
                },
                dataType: 'json',
                headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                success: function (data) {
                    if (data == "1") {
                        window.location.href = "/";
                    } else {
                        logMessage(data, "error", "#login-error-container");
                    }
                }, error: function (data) {
                    logMessage(data, "error", "#login-error-container");
                }
            });
        });

        $("#register-button").click(function() {
            $.ajax({
                url: '/register',
                type: 'POST',
                data: {
                    "name" : $("#register-name").val(),
                    "email" : $("#register-email").val(),
                    "password" : $("#register-password").val(),
                    "password_confirmation" : $("#register-password-conf").val()
                },
                dataType: 'json',
                headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                success: function (data) {
                    if (data == "1") {
                        window.location.href = "/";
                    } else {
                        logMessage(data, "error", "#register-error-container");
                    }
                }, error: function (data) {
                    logMessage(data, "error", "#register-error-container");
                }
            });
        });

        $("#recover-button").click(function() {
            $.ajax({
                url: '/password/email',
                type: 'POST',
                data: {
                    "email" : $("#register-email").val()
                },
                dataType: 'json',
                headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                success: function (data) {
                    if (data == "1") {
                        logMessage("A password recover email has been sent.", "success");
                    } else {
                        logMessage(data, "error", "#recover-error-container");
                    }
                }, error: function (data) {
                    logMessage(data, "error", "#recover-error-container");
                }
            });
        });

        //Called from search form
        function search() {
            var query = $("#map-search").val();
            window.location.replace("/map/" + encodeURI(query));
            return false; //In the case of a submit button
        }
    </script>
@stop
