@extends ("layouts.layout")

@section('title', 'Leaftopia')
@section('javascript') <script src="/assets/js/listings.js"></script> @stop

@section('content')
    <div class="header-dark">
        <div class="container">
            @include('layouts.header')
        </div>
    </div>
    <div class="dark-transp">
        <div class="listing-header">
            <div class="container">
                <h4>Frequently Asked Questions</h4>
            </div>
        </div>
        <div id="live-map"></div>
    </div>
    <section class="dark-text">
        <div class="container">
            <h3>How do I claim a dispensary?</h3>
            <p>If you've found a dispensary on our website that you own we first need to verify that you own it.
            To verify, please send us an email with your name, phone number, and a best time to contact you so
            that we may speak with you directly. Upon verifying that you own the listing you will be able to edit
            details such as menu items, contact details, description, etc.</p>
            <br />
            <h3>How do I contact Leaftopia?</h3>
            <p>If you have a question please send us an email at: info@leaftopia.com.</p>
        </div>
    </section>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCV0UScujyBhP1js2g5oLjV_ss9mOJiRc&callback=initMap" async defer></script>
    <script>
        var map;
        function initMap() {
            getCurrentLocation(function (loc) {
                var latlng = loc.split(",");
                var lat = parseFloat(latlng[0]);
                var lng = parseFloat(latlng[1]);

                //noinspection JSUnresolvedVariable,JSUnresolvedFunction
                map = new google.maps.Map(document.getElementById('live-map'), {
                    center: {lat: lat, lng: lng},
                    zoom: 14
                });
            });
        }
    </script>
@stop