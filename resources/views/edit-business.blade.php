@extends ("business", ["editing" => true])

@section ("form-title")Edit {{ $listing->name }}@endsection
@section ("listing_name"){{ $listing->name }}@endsection
@section ("listing_logo"){{ $listing->avatar }}@endsection
@section ("listing_address"){{ $listing->address }}@endsection
@section ("listing_city"){{ $listing->city }}@endsection
@section ("listing_zip"){{ $listing->zip_code }}@endsection

@section ("menu_items")
    @foreach ($menuItems as $item)
        <div class="menu-item-container">
            <button class="remove-menu-item icon-padding-right"><span class="fa fa-close"></span>Remove</button>
            <div class="row">
                <span class="item-slug" slug="{{ $item->item_slug }}"></span>
                <div class="six columns">
                    <label>Item Type</label>
                    <?php $t = $item->type ?>
                    <select id="select-item-type" class="full-width" name="item-type[]">
                        <option value="Product" {{ $t == "Product" ? "selected" : "" }}>Product</option>
                        <option value="Indica" {{ $t == "Indica" ? "selected" : "" }}>Indica</option>
                        <option value="Preroll" {{ $t == "Preroll" ? "selected" : "" }}>Preroll</option>
                        <option type="Sativa" {{ $t == "Sativa" ? "selected" : "" }}>Sativa</option>
                        <option type="Hybrid" {{ $t == "Hybrid" ? "selected" : "" }}>Hybrid</option>
                    </select>
                    <label>Item Name</label>
                    <input class="form-control full-width" value="{{ $item->name }}" name="item-name[]" type="text" placeholder="Enter an item name"
                           data-validation-error-msg="Names must be between 0 and 225 characters." data-validation="length required"
                           data-validation-length="max225" data-validation-error-msg-container="#item-name-error-container"/>
                    <label class="error-message" id="item-name-error-container"></label>
                    <label>Item Description</label>
                                <textarea id="item-description" name="item-description[]" class="full-width" placeholder="Enter a description" cols="25"
                                          data-validation-error-msg="Your description must be shorter than 2,500 characters" data-validation="length"
                                          data-validation-length="max2500" data-validation-error-msg-container="#item-description-error-container">
                                    {{ $item->body }}
                                </textarea>
                    <label class="error-message" id="item-description-error-container"></label>
                    <label>Item Image</label>
                    <div class="row">
                        <div class="six columns">
                            <input name="item-image[]" type="file" accept="image/*" preview=".item-image-container" />
                        </div>
                        <div class="six columns">
                            <div class="item-image-container" style="background-image: url('{{ $item->image }}')"></div>
                        </div>
                    </div>
                </div>
                <div class="six columns">
                    <?php //Create prices variables
                        $prices = json_decode($item->prices, true);
                        $unit = isset($prices['unit']) ? $prices['unit'] : "";
                        $gram = isset($prices['gram']) ? $prices['gram'] : "";
                        $twogram = isset($prices['two_grams']) ? $prices['two_grams'] : "";
                        $eighth = isset($prices['eighth']) ? $prices['eighth'] : "";
                        $quarter = isset($prices['quarter']) ? $prices['quarter'] : "";
                        $halfounce = isset($prices['half_ounce']) ? $prices['half_ounce'] : "";
                        $ounce = isset($prices['ounce']) ? $prices['ounce'] : "";
                    ?>

                    <label>Prices</label>
                    <div class="prices-container">
                        <div class="row">
                            <div class="one column">
                                <input type="checkbox" name="has-price-unit[]" {{ $unit == "" ? "" : "checked" }} />
                            </div>
                            <div class="three columns">
                                <label>Unit</label>
                            </div>
                            <div class="eight columns">
                                <input class="form-control full-width" value="{{ $unit }}" name="price-unit[]" type="text" placeholder="Enter the cost"
                                       data-validation-error-msg="Please enter a number" data-validation="length number required"
                                       data-validation-length="max225" data-validation-error-msg-container="#price-unit-error-container"/>
                                <label class="error-message" id="price-unit-error-container"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one column">
                                <input type="checkbox" {{ $gram == "" ? "" : "checked" }} name="has-price-gram[]" />
                            </div>
                            <div class="three columns">
                                <label>Gram</label>
                            </div>
                            <div class="eight columns">
                                <input class="form-control full-width" value="{{ $gram }}" name="price-gram[]" type="text" placeholder="Enter the cost"
                                       data-validation-error-msg="Please enter a number" data-validation="length number required"
                                       data-validation-length="max225" data-validation-error-msg-container="#price-gram-error-container"/>
                                <label class="error-message" id="price-unit-gram-container"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one column">
                                <input type="checkbox" {{ $twogram == "" ? "" : "checked" }} name="has-price-2gram[]" />
                            </div>
                            <div class="three columns">
                                <label>Two Grams</label>
                            </div>
                            <div class="eight columns">
                                <input class="form-control full-width" value="{{ $twogram }}" name="price-2gram[]" type="text" placeholder="Enter the cost"
                                       data-validation-error-msg="Please enter a number" data-validation="length number required"
                                       data-validation-length="max225" data-validation-error-msg-container="#price-2gram-error-container"/>
                                <label class="error-message" id="price-2gram-error-container"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one column">
                                <input type="checkbox" {{ $eighth == "" ? "" : "checked" }} name="has-eighth-unit[]" />
                            </div>
                            <div class="three columns">
                                <label>Eighth</label>
                            </div>
                            <div class="eight columns">
                                <input class="form-control full-width" value="{{ $eighth }}" name="price-eighth[]" type="text" placeholder="Enter the cost"
                                       data-validation-error-msg="Please enter a number" data-validation="length number required"
                                       data-validation-length="max225" data-validation-error-msg-container="#price-eighth-error-container"/>
                                <label class="error-message" id="price-eighth-error-container"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one column">
                                <input type="checkbox" {{ $quarter == "" ? "" : "checked" }} name="has-price-quarter[]" />
                            </div>
                            <div class="three columns">
                                <label>Quarter</label>
                            </div>
                            <div class="eight columns">
                                <input class="form-control full-width" value="{{ $quarter }}" name="price-quarter[]" type="text" placeholder="Enter the cost"
                                       data-validation-error-msg="Please enter a number" data-validation="length number required"
                                       data-validation-length="max225" data-validation-error-msg-container="#price-quarter-error-container"/>
                                <label class="error-message" id="price-quarter-error-container"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one column">
                                <input type="checkbox" {{ $halfounce == "" ? "" : "checked" }} name="has-price-half-ounce[]" />
                            </div>
                            <div class="three columns">
                                <label>Half Ounce</label>
                            </div>
                            <div class="eight columns">
                                <input class="form-control full-width" value="{{ $halfounce }}" name="price-half-ounce[]" type="text" placeholder="Enter the cost"
                                       data-validation-error-msg="Please enter a number" data-validation="length number required"
                                       data-validation-length="max225" data-validation-error-msg-container="#price-half-ounce-error-container"/>
                                <label class="error-message" id="price-half-ounce-error-container"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one column">
                                <input type="checkbox" {{ $ounce == "" ? "" : "checked" }} name="has-price-ounce[]" />
                            </div>
                            <div class="three columns">
                                <label>Ounce</label>
                            </div>
                            <div class="eight columns">
                                <input class="form-control full-width" value="{{ $ounce }}" name="price-ounce[]" type="text" placeholder="Enter the cost"
                                       data-validation-error-msg="Please enter a number" data-validation="length number required"
                                       data-validation-length="max225" data-validation-error-msg-container="#price-ounce-error-container"/>
                                <label class="error-message" id="price-ounce-error-container"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
        </div>
    @endforeach
@endsection

<!-- Store slug -->
<span class="slug" slug="{{ $listing->slug }}"></span>