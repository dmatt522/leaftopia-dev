<div class="row">
    <div class="one column">
        <input type="checkbox" checked name="{{ strtolower($day) }} open" value="is-open" title="Check if open" />
    </div>
    <div class="three columns">
        <label>{{ $day }}</label>
    </div>
    <div class="three columns">
        <select class="full-width" name="{{ strtolower($day) }} o">
            <option value="12a">12 AM</option>
            <option value="1a">1 AM</option>
            <option value="2a">2 AM</option>
            <option value="3a">3 AM</option>
            <option value="4a">4 AM</option>
            <option value="5a">5 AM</option>
            <option value="6a">6 AM</option>
            <option value="7a">7 AM</option>
            <option value="8a">8 AM</option>
            <option value="9a">9 AM</option>
            <option value="10a">10 AM</option>
            <option value="11a">11 AM</option>
            <option value="12p">12 PM</option>
            <option value="1p">1 PM</option>
            <option value="2p">2 PM</option>
            <option value="3p">3 PM</option>
            <option value="4p">4 PM</option>
            <option value="5p">5 PM</option>
            <option value="6p">6 PM</option>
            <option value="7p">7 PM</option>
            <option value="8p">8 PM</option>
            <option value="9p">9 PM</option>
            <option value="10p">10 PM</option>
            <option value="11p">11 PM</option>
        </select>
    </div>
    <div class="two columns">
        <label style="text-align: center;">to</label>
    </div>
    <div class="three columns">
        <select class="full-width" name="{{ strtolower($day) . " c" }}">
            <option value="12a">12 AM</option>
            <option value="1a">1 AM</option>
            <option value="2a">2 AM</option>
            <option value="3a">3 AM</option>
            <option value="4a">4 AM</option>
            <option value="5a">5 AM</option>
            <option value="6a">6 AM</option>
            <option value="7a">7 AM</option>
            <option value="8a">8 AM</option>
            <option value="9a">9 AM</option>
            <option value="10a">10 AM</option>
            <option value="11a">11 AM</option>
            <option value="12p">12 PM</option>
            <option value="1p">1 PM</option>
            <option value="2p">2 PM</option>
            <option value="3p">3 PM</option>
            <option value="4p">4 PM</option>
            <option value="5p">5 PM</option>
            <option value="6p">6 PM</option>
            <option value="7p">7 PM</option>
            <option value="8p">8 PM</option>
            <option value="9p">9 PM</option>
            <option value="10p">10 PM</option>
            <option value="11p">11 PM</option>
        </select>
    </div>
</div>